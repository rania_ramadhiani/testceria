package com.example.ceria.network

import com.example.ceria.BuildConfig



object API {
    const val v1 = "v1/"
    const val BASE_URL = BuildConfig.BASE_URL
    //const val APIKEY = BuildConfig.APIKEY
    //const val ENV = BuildConfig.ENV

    const val SEARCH = "search"
    const val USERS = "users"

    //const val SIGNUP = v1 + "registrations/initiate" + APIKEY
}