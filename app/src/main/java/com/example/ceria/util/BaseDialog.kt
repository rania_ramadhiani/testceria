package com.example.ceria.util

import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.example.ceria.databinding.DialogBaseBinding
import com.example.ceria.databinding.DialogBaseYesnoBinding

class BaseDialog {
    companion object {
        fun simpleDialog(
            view: View,
            imageId: Int?,
            title: String,
            desc: String,
            buttonCaption: String,
            isCloseable: Boolean
        ): AlertDialog {
            val dialogView =
                DialogBaseBinding.inflate(LayoutInflater.from(view.context), null, false)
            val simpleDialog = AlertDialog.Builder(view.context).create()
            dialogView.apply {
                if (imageId != null) {
                    ivBaseDialog.setImageResource(imageId)
                    UtilFunction.stateViewVisible(ivBaseDialog)
                } else
                    UtilFunction.stateViewGone(ivBaseDialog)

                if (isCloseable) {
                    ivCloseBaseDialog.visibility = View.VISIBLE
                    ivCloseBaseDialog.setOnClickListener {
                        simpleDialog.dismiss()
                    }
                } else
                    ivCloseBaseDialog.visibility = View.GONE

                tvBaseDialog.text = title
                tvDescBaseDialog.text = desc
                btnBaseDialog.text = buttonCaption
            }

            simpleDialog.setView(dialogView.root)
            simpleDialog.setCancelable(false)
            return simpleDialog
        }

        fun yesnoDialog(
            view: View,
            imageId: Int?,
            title: String,
            desc: String,
            buttonYes: String,
            buttonNo: String
        ): AlertDialog {
            val dialogView =
                DialogBaseYesnoBinding.inflate(LayoutInflater.from(view.context), null, false)
            val yesnoDialog = AlertDialog.Builder(view.context).create()
            dialogView.apply {
                if (imageId != null) {
                    ivYesnoBaseDialog.setImageResource(imageId)
                    UtilFunction.stateViewVisible(ivYesnoBaseDialog)
                } else
                    UtilFunction.stateViewGone(ivYesnoBaseDialog)

                tvYesnoBaseDialog.text = title
                tvDescYesnoBaseDialog.text = desc
                btnYesYesnoBaseDialog.text = buttonYes
                btnNoYesnoBaseDialog.text = buttonNo
            }

            yesnoDialog.setView(dialogView.root)
            yesnoDialog.setCancelable(false)
            return yesnoDialog
        }


    }
}